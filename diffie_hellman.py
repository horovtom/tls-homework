import hashlib
import random
from typing import Optional

from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

from Agent import Agent
from util import *


class Person:
    def __init__(self, p, g):
        self.p = p
        self.g = g
        self.a = random.randrange(self.p)
        self.secret = None
        self.key = None

    def get_secret(self):
        return self.secret

    def get_key(self):
        self.key = self.key or self.__calculate_key()
        return self.key

    def __calculate_key(self):
        hasher = hashlib.sha1()
        hasher.update(int2bin(self.get_secret()))
        return hasher.digest()[:BLOCK_SIZE]

    def get_my_part(self):
        return pow(self.g, self.a, self.p)

    def accept_other_part(self, number):
        self.secret = pow(number, self.a, self.p)

    def encode(self, txt: str):
        message = txt2bin(txt)
        iv = get_random_bytes(BLOCK_SIZE)
        message = self.pad(message)
        cipher = AES.new(self.get_key(), AES.MODE_CBC, iv)
        return iv + cipher.encrypt(message)

    def decode(self, message: bytes):
        iv = message[:BLOCK_SIZE]
        cipher = AES.new(self.get_key(), AES.MODE_CBC, iv)
        return bin2txt(self.unpad(cipher.decrypt(message[BLOCK_SIZE:])))

    def pad(self, x: bytes) -> bytes:
        length = len(x)
        remainder = 16 - (length % 16)
        to_append = int2bin(remainder) * remainder
        return x + to_append

    def unpad(self, y: bytes) -> Optional[bytes]:
        pad_len = y[-1]
        # Check if same
        for i in y[-pad_len:]:
            if i != pad_len:
                return None

        return y[:-pad_len]


def key_exchange(p, g, alice, bob):
    public_data = (p, g)
    # Set alice's public data to the provided values
    alice.receive_public_data(*public_data)
    bob.receive_public_data(*alice.send_public_data())
    alice.receive_public_key(bob.send_public_key())
    bob.receive_public_key(alice.send_public_key())

    # Extracting info from the agent, it should not be possible from the outside world
    return alice._get_key(), bob._get_key()



def part1and2(p, g):
    print("\tPart 1 and 2:")
    print("\t\tDH key exchange for p=37, g=5")
    alice = Agent()
    bob = Agent()
    alice_key, bob_key =key_exchange(37, 5, alice, bob)
    print("\t\tAlice's key: {}".format(alice_key))
    print("\t\t  Bob's key: {}".format(bob_key))

    print("\t\tDH key exchange for \n\t\t\tp={}\n\t\t\tg={}".format(p, g))
    alice = Agent()
    bob = Agent()
    alice_key, bob_key = key_exchange(p, g, alice, bob)
    print("\t\tAlice's key: {}".format(alice_key))
    print("\t\t  Bob's key: {}".format(bob_key))



if __name__ == "__main__":
    part1and2()
