import math
from typing import Tuple

import util


class RsaAgent:
    def __init__(self, p=None, q=None, e=None):
        self.p = p or util.get_random_prime()
        self.q = q or util.get_random_prime()
        self.n = self.p * self.q
        # λ(n) = lcm(λ(p), λ(q))
        # Where λ(p) = φ(p) = p-1 and λ(q) = φ(q) = q-1
        self.phi_n = math.lcm(self.p - 1, self.q - 1)
        self.e = e or util.get_random_number(not_divisible_by=self.phi_n, max=self.phi_n)
        self.d = util.invmod(self.e, self.phi_n)

    def get_public_key(self):
        return self.e, self.n

    def encrypt(self, data: bytes, public_key) -> Tuple[int, int]:
        # Convert data to int
        # Actually there should be some padding here for it to be secure I guess.
        res = int.from_bytes(data, byteorder='big', signed=False)
        return self.encrypt_int(res, public_key), len(data)

    def decrypt(self, encrypted: Tuple[int, int]) -> bytes:
        res = self.decrypt_int(encrypted[0])
        # Convert res to bytes
        return res.to_bytes(encrypted[1], byteorder='big', signed=False)

    def encrypt_int(self, number: int, public_key) -> int:
        e, n = public_key
        return pow(number, e, n)

    def decrypt_int(self, number: int) -> int:
        return pow(number, self.d, self.n)


if __name__ == "__main__":
    agent_a = RsaAgent()
    agent_b = RsaAgent()
    encrypted = agent_a.encrypt(b"I am a bunny", agent_b.get_public_key())
    print(agent_b.decrypt(encrypted))
