import math
import random
from typing import Union, Iterator

import Crypto.Random
import Crypto.Util.number

encoding = "utf-8"

BLOCK_SIZE = 16
P = 0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF
G = 2


def bin2txt(bin: bytes) -> str:
    return str(bin, encoding)


def bin2hex(bin: bytes) -> str:
    return bin.hex()


def bin2int(bin: bytes) -> int:
    return hex2int(bin2hex(bin))


def hex2txt(hexa: str) -> str:
    return bin2txt(hex2bin(hexa))


def hex2bin(hexa: str) -> bytes:
    if len(hexa) % 2 != 0:
        hexa = "0" + hexa
    return bytes.fromhex(hexa)


def hex2int(hexa: str) -> int:
    return int(hexa, 16)


def txt2bin(txt: str) -> bytes:
    return str.encode(txt, encoding)


def txt2hex(txt: str) -> str:
    return bin2hex(txt2bin(txt))


def int2hex(num: int) -> str:
    return "{:02x}".format(num)


def int2bin(num: int) -> bytes:
    return hex2bin(int2hex(num))


def xor_together(*args: Union[bytes, Iterator]) -> bytes:
    res = args[0]

    for arg in args[1:]:
        res = bytes(k ^ t for k, t in zip(res, arg))

    return res


def invmod(a, n):
    # Uses EEA
    a %= n

    # We compute a^-1 mod n using extended euclidian algo
    # So we calculate gcd(n, a) = r using this algorithm. If r is 1, then we can retrieve the result from the last value of ti
    # Else we might have to do something else...

    # Fill with init values
    # qi, ri, ti
    tab = [[None, n, 0], [None, a, 1]]

    while tab[-1][1] > 1:
        last1 = tab[-2]
        last2 = tab[-1]
        # new_qi = last1[1] // last2[1]
        # new_ri = last1[1] - new_qi * last2[1]
        # new_ti = last1[2] - new_qi * last2[2]
        tab.append([new_qi:=last1[1] // last2[1], last1[1] - new_qi * last2[1], (last1[2] - new_qi * last2[2]) % n])

    if tab[-1][1] == 1:
        return tab[-1][2] % n

    return None



def get_random_number(not_divisible_by, max):
    # Certainly not optimal
    while True:
        number = random.randint(3, max)
        if math.gcd(number, not_divisible_by) == 1:
            return number


def get_random_prime(bits=64):
    return Crypto.Util.number.getPrime(bits, randfunc=Crypto.Random.get_random_bytes)
