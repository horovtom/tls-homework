import hashlib
import random
from typing import Optional

from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

from util import *

class Agent:
    def __init__(self, msg=None):
        self.msg = msg
        self.p = 0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF
        self.g = 2
        self.A = None
        self.a = None
        self.secret = None
        self.key = None

    def receive_public_data(self, p, g):
        self.p = p
        self.g = g
        self.a = random.randrange(self.p)
        self.A = pow(self.g, self.a, self.p)

    def send_public_data(self):
        return self.p, self.g

    def send_public_key(self):
        return self.A

    def receive_public_key(self, key):
        self.secret = pow(key, self.a, self.p)

    def send_message(self):
        message = txt2bin(self.msg)
        iv = get_random_bytes(BLOCK_SIZE)
        message = self.pad(message)
        # We actually already implemented CBC as a part of our previous assignment in our group.
        # I am not including it here as it would be unnecessarily complex and I think the main point of this exercise is
        # to try and implement it, which I have already done anyway.
        cipher = AES.new(self._get_key(), AES.MODE_CBC, iv)
        return iv + cipher.encrypt(message)

    def receive_message(self, message):
        iv = message[:BLOCK_SIZE]
        cipher = AES.new(self._get_key(), AES.MODE_CBC, iv)
        self.msg = bin2txt(self.unpad(cipher.decrypt(message[BLOCK_SIZE:])))

    def pad(self, x: bytes) -> bytes:
        length = len(x)
        remainder = 16 - (length % 16)
        to_append = int2bin(remainder) * remainder
        return x + to_append

    def unpad(self, y: bytes) -> Optional[bytes]:
        pad_len = y[-1]
        # Check if same
        for i in y[-pad_len:]:
            if i != pad_len:
                return None

        return y[:-pad_len]

    def _get_key(self):
        self.key = self.key or self.__calculate_key()
        return self.key

    def __calculate_key(self):
        hasher = hashlib.sha1()
        hasher.update(int2bin(self.secret))
        return hasher.digest()[:BLOCK_SIZE]
