from Agent import Agent


class HackedAgent(Agent):
    def get_message(self):
        return self.msg

    def set_message(self, message):
        self.msg = message


class MITM:
    def __init__(self):
        self.agents = [HackedAgent(), HackedAgent()]
        # Arbitrary order of operations, each receive_send function should be split in two and they would select a respective agent.
        # This is only a simulation and it works like this, somewhat easily
        self.order = [0, 1, 1, 0]
        self.msg = None

    def __get_current_agent(self):
        current = self.order.pop(0)
        self.order.append(current)
        return self.agents[current]

    def receive_public_data(self, *public_data):
        self.__get_current_agent().receive_public_data(*public_data)

    def send_public_data(self):
        return self.__get_current_agent().send_public_data()

    def receive_public_key(self, public_key):
        self.__get_current_agent().receive_public_key(public_key)

    def send_public_key(self):
        return self.__get_current_agent().send_public_key()

    def intercept_message(self, message):
        # Receive message from one
        agent1 = self.__get_current_agent()
        agent1.receive_message(message)
        self.msg = agent1.get_message()
        # Send it on its way to another
        agent2 = self.__get_current_agent()
        agent2.set_message(self.msg)
        return agent2.send_message()
