import itls101
import tls101
from RsaAgent import RsaAgent
from diffie_hellman import part1and2
from util import G, P
from util import invmod


def test_invmod():
    assert invmod(19, 1212393831) == 701912218
    assert invmod(13, 91) is None
    print("\t\t\tIt was OK")


def test_rsa_agents():
    p = 13604067676942311473880378997445560402287533018336255431768131877166265134668090936142489291434933287603794968158158703560092550835351613469384724860663783
    q = 20711176938531842977036011179660439609300527493811127966259264079533873844612186164429520631818559067891139294434808806132282696875534951083307822997248459
    e = 3
    alice = RsaAgent()
    bob = RsaAgent(p, q, e)
    quote = b'To be or not to be, that is the question!'
    data = alice.encrypt(quote, bob.get_public_key())
    assert bob.decrypt(data) == quote
    print("\t\t\tIt was OK")


part1and2(P, G)
print("\n\tPart 3 and 4:")
print("\t\trunning tls101:")
tls101.run()
print("\n\tPart 5:")
print("\t\trunning itls101:")
itls101.run()
print("\n\tPart 6:")
print("\t\tTesting invmod...")
test_invmod()
print("\t\tTesting Rsa agents")
test_rsa_agents()
